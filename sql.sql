CREATE SCHEMA `sportisimo` ;
USE `sportisimo`;

CREATE TABLE `brand` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name` VARCHAR(255) NOT NULL UNIQUE
) ENGINE=InnoDB CHARSET=utf8;

INSERT INTO `brand` (`name`) VALUES
('Nike'),
('Adidas'),
('Puma'),
('Jordan'),
('Oneil'),
('DC'),
('Head'),
('A3'),
('Gravity'),
('Larsa'),
('Klimatex'),
('Willard'),
('Loap');