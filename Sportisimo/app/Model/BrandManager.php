<?php

namespace App\Model;
use Nette;

class BrandManager 
{
    use Nette\SmartObject;

    private Nette\Database\Explorer $database;

    public function __construct(Nette\Database\Explorer $database)
    {
        $this->database = $database;
    }
    
    /**
     * metoda ukladá novou značku do db
     * @param array $data
     * @return void
     */
    public function saveBrand(object $data): void
    {
       $this->database->table('brand')->insert($data);
    }
    
    /**
     * metoda vrací 1 značku z databáze
     * @param int $brandId
     * @return \Brand
     * @throws BrandNotFoundException
     */
    public function getBrand(int $brandId): \Brand
    {
        $row = $this->database->table('brand')->get($brandId);
        
        if($row === null)
        {
            throw new BrandNotFoundException();
        }
        
        $brand = new \Brand();
        $brand->setId($row['id']);
        $brand->setName($row['name']);
        return $brand;
        
    }
    
    /**
     * metoda slouží k aktualizaci značky
     * 
     * @param string $brandId
     * @param type $data
     * @return void
     * @throws BrandNotFoundException
     */
    public function updateBrand(string $brandId ,$data): void
    {
        $row = $this->database->table('brand')->get($brandId);
        
        if($row === null)
        {
            throw new BrandNotFoundException();
        }
        
        $row->update($data);
           
    }
   
    /**
     * metoda smaže značku z db
     * 
     * @param int $brandId
     * @return void
     * @throws BrandNotFoundException
     */
    public function deleteBrand(int $brandId): void
    {      
        $this->database->table('brand')->get($brandId)->delete();
    }
    
    /**
     * metoda vrací všechny značky z db
     * @param int $limit
     * @param int $offset
     * @return array
     */    
    public function findBrands(int $limit, int $offset): array
    {
        $rows = $this->database->table('brand')->limit($limit, $offset);
        
        $brands = array();
        foreach ($rows as $row)
        {
            $brand = new \Brand();
            $brand->setId($row['id']);
            $brand->setName($row['name']);
            $brands[] = $brand;
        }
        
        return $brands;
    }
    
    /**
     * metoda vrací počet značek v db
     * @return int
     */
    public function getBrandsCount(): int
    {
        return $this->database->table('brand')->count();
    }
    
    /**
     * metoda vrací seřazené značky
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function findSortedBrandsAsc(int $limit, int $offset): array
    {
        $rows = $this->database->table('brand')->order('name')->limit($limit, $offset);
        
        $brands = array();
        foreach ($rows as $row)
        {
            $brand = new \Brand();
            $brand->setId($row['id']);
            $brand->setName($row['name']);
            $brands[] = $brand;
        }
        
        return $brands;
    }
    
    /**
     * metoda vrací seřazené značky
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function findSortedBrandsDesc(int $limit, int $offset): array
    {
        $rows = $this->database->table('brand')->order('name DESC')->limit($limit, $offset);
        
        $brands = array();
        foreach ($rows as $row)
        {
            $brand = new \Brand();
            $brand->setId($row['id']);
            $brand->setName($row['name']);
            $brands[] = $brand;
        }
        
        return $brands;
    }
    
    
}
