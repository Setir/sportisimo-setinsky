<?php


declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\BrandManager;
use Nette\Application\UI\Form;

class BrandPresenter extends Nette\Application\UI\Presenter
{
    private BrandManager $brandManager;
    
    
    public function __construct(BrandManager $brandManager)
    {
        $this->brandManager = $brandManager;
    }
    
    /**
     * metoda předává data(značky) do view Brand:Default
     * @param int $page
     * @param string|null $sort
     * @param int|null $pageSize
     * @return void
     */
    public function actionDefault(int $page = 1, ?string $sort = null, ?int $pageSize = 5 ): void
    {   
        $brandCount = $this->brandManager->getBrandsCount(); 
        $lastPage = 0; 
        
        $paginator = new Nette\Utils\Paginator;
        $paginator->setItemCount($brandCount); 
        $paginator->setItemsPerPage($pageSize); 
        $paginator->setPage($page);
        
        if($sort === "asc")
        {   $sorting = "asc";
            $sizePage = $pageSize;
            $brands = $this->brandManager->findSortedBrandsAsc($paginator->getLength(), $paginator->getOffset());
        }
        else if($sort === "desc")
        {   
            $sorting = "desc";
            $sizePage = $pageSize;
            $brands = $this->brandManager->findSortedBrandsDesc($paginator->getLength(), $paginator->getOffset());
        }
        else
        { 
            $sorting = null;
            $sizePage = $pageSize;
            $brands = $this->brandManager->findBrands($paginator->getLength(), $paginator->getOffset());
        }
        
        $this->template->brands = $brands;
        $this->template->sizePage = $sizePage;
        $this->template->sorting = $sorting;
        $this->template->paginator = $paginator; 
        $this->template->lastPage = $lastPage;
    }
    
    /**
     * metoda vytvoří a předá form view
     * @return Form
     */
    protected function createComponentBrandForm(): Form
    {
        
	$form = new Form;
	$form->addText('name', 'Name:')
            ->setRequired("Enter name, name cant be empty")
            ->addRule($form::MIN_LENGTH, "Name must have atleast 3 letters", 3)
            ->setMaxLength(15);
	$form->addSubmit('send', 'Save');
        $form->onSuccess[] = [$this, 'brandFormSucceeded'];
	return $form;
    }
    
    /**
     * Metoda slouží ke zpracování dat z formuláře
     * @param Form $form
     * @param type $data
     * @return void
     */
    public function brandFormSucceeded(Form $form, $data): void
    {
        $brandId = $this->getParameter('brandId');
        
        try 
        {
            if ($brandId)
            {
		$this->brandManager->updateBrand($brandId,$data);
                $this->flashMessage("A Brand was edited", 'success');
                $this->redirect('default');
            } 
            else
            {   
                $this->brandManager->saveBrand($data);
                $this->flashMessage("A new Brand was created", 'success');
                $this->redirect('default');
            }
        } 
        catch(Nette\Database\UniqueConstraintViolationException $e) 
        {
            $form->addError("Name is used");
	}
    }
    
    /**
     * metoda slouží k editaci značky
     * @param int $brandId
     * @return void
     */
    public function actionEdit(int $brandId): void
    {
        try 
        {
            $brand = $this->brandManager->getBrand($brandId);
        } 
        catch (\BrandNotFoundException $brandNotFoundException) 
        {
            $brandNotFoundException->errorMessage("Brand not found");
        }
	
	$this['brandForm']->setDefaults(array('name' => $brand->getName()));
    }
    
    /**
     * metoda slouží k mazaní značek
     * @param int $brandId
     * @return void
     */
    public function actionDelete(int $brandId): void
    {
        try 
        {
            $this->brandManager->getBrand($brandId);
        } 
        catch (\BrandNotFoundException $brandNotFoundException) 
        {
            $brandNotFoundException->errorMessage("Brand not found");
        }
        
        $this->brandManager->deleteBrand($brandId);
        $this->flashMessage("Brand was deleted", 'success');
	$this->redirect('default');
    }
}
