<?php


class Brand 
{
    use Nette\SmartObject;
    
    private int $id;
    private ?string $name = "null";

    function setId(int $id): void {
        $this->id = $id;
    }

    function setName(string $name): void {
        $this->name = $name;
    }
    
    function getId(): int {
        return $this->id;
    }

    function getName(): string {
        return $this->name;
    }
}
